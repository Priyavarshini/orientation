**GIT**


Git is a version control (VCS) system for tracking changes to projects.

![pic](/uploads/fc57abee73ecdbd9ea80a9cbbfbf7f0f/pic.png)

**TERMINOLOGY**




**Repository**




It can be termed as Repo.
These are Files/ Fol0ders (code files) that are tracked by GIT.





**GIT Lab**




Storage of GIT Repo.





**Commit**




This is similar as SAVING our work ie. files or codes.
Exist in LOCAL MACHINE unless it is Pushed to Remote Repository.





**Push**





Synching commits to GIT lab.





**Branch**




These are Instances of codes

Master Branch is the main software.





**Merge**




Integrating two Branches
When the branch is Bug free it is merged to Primary codebase(Master Branch).





**Clone**




Copying Repo and paste on Local Machine.⁸





**Fork**




To get a new Repo under your name.



**Git Stages:**


Modified: changed file, but not commited

Staged: file ready to commit

Comitted: Data stored in local repo

**TREES OF SOFTWARE CODES/REPOSITORIES**

Work Space.

Staging.

Local Repository.

Remote Repository.


![Repo](/uploads/d84c89dae4d36145a8fa2dc6ffe885b9/Repo.png)

**Some of the commands in Git workflow include:**

**Clone repo:**

'$ git clone '

**New branch:**

'$ git checkout master' 

'$ git checkout -b '

**Add untracked files:**

'$ git add'

**Commit**

'$ git commit -sv'

**Push:**

'$ git push origin '

![68747470733a2f2f6d69726f2e6d656469756d2e636f6d2f6d61782f3837352f312a61396344734b4947304a46586b4e30795666473832512e706e67](/uploads/238f2ffc6fed7b0f50af71b38824ccc0/68747470733a2f2f6d69726f2e6d656469756d2e636f6d2f6d61782f3837352f312a61396344734b4947304a46586b4e30795666473832512e706e67.png)

**Git Workflow**


![Git](/uploads/42ee12277c5286d53ebaaf56fe3060f8/Git.png)


**Summary of Essential Git Commands**


```
git add: _add a changed file or a new file to be committed_
git diff: _see the changes between the current version of a file and the version of the file t recently committed_
git commit: _commit changes to the history_
git log: _show the history for a project_
git revert: _undo a change introduced by a specific commit_
git checkout: _switch branches or move within a branch_
git clone: _clone a remote repository_
git pull: _pull changes from a remote repoository_
```

![Workflow](/uploads/d9c028881d78fe82e196d0cd2ad4b2e6/Workflow.jpg)

**Workflow With Commands**
![pic1.svg](/uploads/73cc90b832734bb7887131129934662f/pic1.svg)

