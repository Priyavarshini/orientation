**Docker overview**


Docker is an open platform for developing, shipping, and running applications.

Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.


![What-Is-Docker](/uploads/cae38ceff8802245af16830b81cc2ab5/What-Is-Docker.png)

**The Docker platform**


Docker provides the ability to package and run an application in a loosely isolated environment called a container.


![pic5](/uploads/6726858c853e5ac9167af942630da6d6/pic5.jpg)


**1. Docker Image**


Docker Image is a package of all the tools one would need to run applications.

image contains requirements to run application and is deployed to Docker as container

Requirements:


-code 

-runtime

-libraries

-env variables

-config files


![doc](/uploads/a58548a3131d1c00dd0025ef9c91e548/doc.png)  



**2. Docker Containers**


Each have a specific job, their own operating system, their own isolated cpu
processes, memory and cpu resources.

- Running docker image

- Can create multiple containers

![pic4](/uploads/9c3a292e8e3895bf850add607209da3b/pic4.png)


**Working of Container**


- Containers are instance of images ie use them to create an enviroment and run applications.


- Containers are the execution part of docker analogues to "process"


**DOCKER HUB:**


Platform where Docker Images and Containers can be shared


![pic3](/uploads/fff21de16d4cfb53382da61ea6285589/pic3.jpg)


**CONTAINERS VS VIRTUAL MACHINES**


- Containers provides a way to  virtualize OS so that multiple workloads can be run on single OS instances.


- where as in VM the hardware is being virtualize to run multiple OS instances.

![pic2](/uploads/44382f4f2f84e69084a3e1cbc220f76d/pic2.png)


**Docker Engine**


- Its an opens source containerization technology that build and containerize our app .

 
 
 
- Act as Client-server application.
 
 
 
 
- The Docker Engine creates server side Daemon process that hosts images ,containers,networks and storage volume




- It also provide Client side  Command line interface (Cli)that enables us to interact with daemon through Docker Engine API..


![engine](/uploads/bd249fb9985f51f55cd72069e444070e/engine.png)


**Docker basic commands**


```
docker ps :_The docker ps command allows us to view all the containers that are running on the Docker Host._

docker start:_This command starts any stopped container(s)._

docker stop:_This command stops any running container(s)_

docker run:_This command creates containers from docker images._

docker rm:_This command deletes the containers_
 
```

